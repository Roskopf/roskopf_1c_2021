/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Roskopf, Ana Evangelina
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"
#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/
#define BAUD 115200
int16_t valor_analogico;
bool flag_timer= false;
#define BUFFER_SIZE 231
#define Fmax 50
#define Fmin 5
#define Fpaso 5
#define DT 0.002

float Sfiltrada=0;
float Sfiltrada_antes=0;
float Entrada=0;
float Fcorte=25;
float rc=0;
float alfa=0;

bool flag_adc= false;
bool flag_dac= false;
bool filtro=false;

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void FuncionTimer(){
	AnalogStartConvertion();
}

void FuncionUart(){

}

void FuncionAnalog(){
	AnalogInputRead(CH1,&valor_analogico);
	UartSendString(SERIAL_PORT_PC, UartItoa(valor_analogico, 10));
	UartSendString(SERIAL_PORT_PC,"\r");
}

void FuncionTimerDAC(){
	flag_dac=true;
}

void Nueva_FuncionTimer(){
	uint8_t i=0;
	AnalogOutputWrite(ecg[i]);
	i++;
	if(i==231)
		i=0;
}

/* Controles teclas */
void tecla1(){
	filtro=true;
}

void tecla2(){
	filtro=false;
}

void tecla3(){

	if (Fcorte >= Fmin) {
		Fcorte = Fcorte - Fpaso;
		rc = 1 /( 2 * (3.14) * Fcorte );
		alfa = DT /( DT + rc );
	}
	else {
		Fcorte = Fcorte;
	}
}

void tecla4(){
	if (Fcorte <= Fmax) {
			Fcorte = Fcorte + Fpaso;
			rc = 1 / ( 2 * (3.14) * Fcorte );
			alfa = DT / ( DT + rc );
		}
	else {
		Fcorte = Fcorte;
	}
}


void FiltroPB (){
	Entrada=valor_analogico;
	if (filtro == true){
		Sfiltrada= Sfiltrada_antes + alfa*(Entrada - Sfiltrada_antes);
		Sfiltrada_antes=Sfiltrada;
	}
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();

	timer_config my_timer={TIMER_A,2, FuncionTimer}; /*fm:500Hz, se coloca 2 xq el timer esta en ms*/
	TimerInit(&my_timer);
	TimerStart(my_timer.timer);

	timer_config my_timerB={TIMER_B,5, Nueva_FuncionTimer};/*fm:200Hz, se coloca 5 xq el timer esta en ms*/
	TimerInit(&my_timerB);
	TimerStart(my_timerB.timer);

	analog_input_config my_analog= { CH1,AINPUTS_SINGLE_READ, FuncionAnalog};
	AnalogInputInit(&my_analog);
	AnalogOutputInit();

	serial_config my_uart ={SERIAL_PORT_PC,BAUD, FuncionUart};
	UartInit(&my_uart);

	SwitchActivInt(SWITCH_1,tecla1);
	SwitchActivInt(SWITCH_2,tecla2);
	SwitchActivInt(SWITCH_3,tecla3);
	SwitchActivInt(SWITCH_4,tecla4);

	uint8_t i=0;
	rc = 1 /( 2 * (3.14) * Fcorte);
	alfa = DT /( DT + rc );

	while(1){
		if (flag_dac == true){
		    if (i < BUFFER_SIZE){
		    	AnalogOutputWrite(ecg[i]);
		    	i++;
		    }/*escribe los valores del ecg al conversor, xq esta activada la bandera dac*/
		    else{
		    	i = 0;
		    }
		    flag_dac = false;
		}
		if (flag_adc == true){
		    UartSendString(SERIAL_PORT_PC, UartItoa(valor_analogico,10));
		    UartSendString(SERIAL_PORT_PC,",");

    		FiltroPB();

    		UartSendString(SERIAL_PORT_PC, UartItoa((uint16_t) Sfiltrada, 10));
    		UartSendString(SERIAL_PORT_PC,"\r");
    		flag_adc=false;
	}
}
}

/*==================[end of file]============================================*/
