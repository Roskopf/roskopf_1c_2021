/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Roskopf, Ana Evangelina
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"
#include "systemclock.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define BAUD 115200
int16_t valor_analogico;
bool flag_timer= false;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void FuncionTimer(){
	AnalogStartConvertion();
}

void FuncionUart(){

}

void FuncionAnalog(){
	AnalogInputRead(CH1,&valor_analogico);
	UartSendString(SERIAL_PORT_PC, UartItoa(valor_analogico, 10));
	UartSendString(SERIAL_PORT_PC,"\r");
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	timer_config my_timer={TIMER_A,2, FuncionTimer}; /*fm:500Hz, se coloca 2 xq el timer esta en ms*/
	TimerInit(&my_timer);

	TimerStart(my_timer.timer);

	analog_input_config my_analog= { CH1,AINPUTS_SINGLE_READ, FuncionAnalog};

	AnalogInputInit(&my_analog);

	serial_config my_uart ={SERIAL_PORT_PC,BAUD, FuncionUart};
	UartInit(&my_uart);

	while(1){

	}
}

/*==================[end of file]============================================*/
