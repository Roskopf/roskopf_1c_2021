/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Roskopf, Ana Evangelina
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"
#include "gpio.h"
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUD 115200
int16_t cm=0;
int16_t in=0;
bool medir= false;
bool hold= false;
bool flag_timer= false;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void tecla_1(){
	if(medir == false){
	   medir = true;
	}
	else {
		medir = false;
	}
}
void tecla_2(){
	if(hold == false){
		hold = true;
	}
	else {hold = false;}
}

void FuncionTimer(){
	flag_timer=true;

	UartSendString(SERIAL_PORT_PC, UartItoa(cm, 10));
	UartSendString(SERIAL_PORT_PC," ");
	UartSendString(SERIAL_PORT_PC,"cm");
	UartSendString(SERIAL_PORT_PC,"\r\n");
}

void EncenderLeds()
{
	if(flag_timer=true){
		if(medir == true){
			cm = HcSr04ReadDistanceCentimeters();
			in = HcSr04ReadDistanceInches();
			if(hold == false){
				Led_distancia(cm);
				DelaySec(1);
				medir=false;
			}
			else{
				LedsOffAll();
				cm=0;
				hold=false;
			}
			flag_timer=false;
		}
	}
}
bool Led_distancia(int16_t distancia_medida)
{
	int16_t dist=distancia_medida;
	if(dist<=30)
	{
		if(dist>20)
		{
    	    LedOn(LED_RGB_B);
    	    LedOff(LED_RGB_G);
    	    LedOff(LED_RGB_R);
    	    LedOn(LED_1);
    	    LedOn(LED_2);
    	    LedOff(LED_3);
		}
		else
		{
			if(10<dist)
			{
	    	    LedOn(LED_RGB_B);
	    	    LedOff(LED_RGB_G);
	    	    LedOff(LED_RGB_R);
	    	    LedOn(LED_1);
	    	    LedOff(LED_2);
	    	    LedOff(LED_3);
			}
			else
			{
	    		LedOn(LED_RGB_B);
	    		LedOff(LED_RGB_G);
	    		LedOff(LED_RGB_R);
	    		LedOff(LED_1);
	    		LedOff(LED_2);
	    		LedOff(LED_3);
			}
		}
	}
	else
	{
	    LedOn(LED_RGB_B);
	    LedOff(LED_RGB_G);
	    LedOff(LED_RGB_R);
	    LedOn(LED_1);
	    LedOn(LED_2);
	    LedOn(LED_3);
	}

	return true;
}

void FuncionUart(){
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC,&dato);
		if ((dato== 'O') || (dato=='o')){ /*lectura de puerto*/
			tecla_1();}
		if ((dato == 'H') || (dato == 'h')){ /*lectura de puerto*/
			tecla_2();}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	LedsInit();

	HcSr04Init(T_FIL2,T_FIL3);

	SwitchesInit();
	SwitchActivInt(SWITCH_1,tecla_1);
	SwitchActivInt(SWITCH_2,tecla_2);

	timer_config my_timer={TIMER_A,1000, FuncionTimer};
	TimerInit(&my_timer);

	TimerStart(my_timer.timer);

	serial_config my_uart ={SERIAL_PORT_PC,BAUD, FuncionUart};
	UartInit(&my_uart);


    while(1){

    	EncenderLeds();

    }

}

/*==================[end of file]============================================*/
