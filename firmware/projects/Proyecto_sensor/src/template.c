/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Roskopf, Ana Evangelina
 *
 */

/*==================[inclusions]=============================================*/
#include "template.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "delay.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/
bool medir= false;
bool hold= false;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void tecla_1(){
	if(medir == false){
	   medir = true;
	}
	else {
		medir = false;
	}
}
void tecla_2(){
	if(hold == false){
		hold = true;
	}
	else {hold = false;}
}

bool Led_distancia(int16_t distancia_medida)
{
	int16_t dist=distancia_medida;
	if(dist<=30)
	{
		if(dist>20)
		{
    	    LedOn(LED_RGB_B);
    	    LedOff(LED_RGB_G);
    	    LedOff(LED_RGB_R);
    	    LedOn(LED_1);
    	    LedOn(LED_2);
    	    LedOff(LED_3);
		}
		else
		{
			if(10<dist)
			{
	    	    LedOn(LED_RGB_B);
	    	    LedOff(LED_RGB_G);
	    	    LedOff(LED_RGB_R);
	    	    LedOn(LED_1);
	    	    LedOff(LED_2);
	    	    LedOff(LED_3);
			}
			else
			{
	    		LedOn(LED_RGB_B);
	    		LedOff(LED_RGB_G);
	    		LedOff(LED_RGB_R);
	    		LedOff(LED_1);
	    		LedOff(LED_2);
	    		LedOff(LED_3);
			}
		}
	}
	else
	{
	    LedOn(LED_RGB_B);
	    LedOff(LED_RGB_G);
	    LedOff(LED_RGB_R);
	    LedOn(LED_1);
	    LedOn(LED_2);
	    LedOn(LED_3);
	}

	return true;
}

bool Led_apagar(){
	LedOff(LED_RGB_R);
	LedOff(LED_RGB_B);
	LedOff(LED_RGB_G);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	int16_t cm=0;
    int16_t in=0;
	uint8_t estado_actual, estado_anterior;
	/*estado_anterior=0;*/


	SystemClockInit();
	LedsInit();
	SwitchesInit();

	/*SwitchActivInt(SWITCH_2,tecla_2);*/

	HcSr04Init(T_FIL2, T_FIL3);


    while(1){
        estado_actual  = SwitchesRead();
        if(estado_actual!= estado_anterior){
        switch(estado_actual){
        	case (SWITCH_1):
				SwitchActivInt(SWITCH_1,tecla_1);
        		estado_anterior=estado_actual;
        	break;
        	case (SWITCH_2):
				SwitchActivInt(SWITCH_2,tecla_2);
        	break;
        }
    }

    if(medir == true){
    	cm = HcSr04ReadDistanceCentimeters();
    	in = HcSr04ReadDistanceInches();
    	if(hold == false){
    		Led_distancia(cm);
    		DelaySec(1);
    		medir=false;
    	}
		else{
			Led_apagar();
			cm=0;
			hold=false;
		}
    }

   }
}

/*==================[end of file]============================================*/
