/* @brief  EDU-CIAA NXP GPIO driver
 * @author Albano Peñalva
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#ifndef TEMPLATE_H_
#define TEMPLATE_H_

//#include <stdint.h>
#include "gpio.h"

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

bool HcSr04Init(gpio_t echo, gpio_t trigger);

/**
 * @brief Delay in milliseconds
 * @param[in] msec milliseconds to be in delay
 * @return None
 */


int16_t HcSr04ReadDistanceCentimeters(void);


/**
 * @brief Delay in microseconds
 * @param[in] usec microseconds to be in delay
 * @return None
 */

int16_t HcSr04ReadDistanceInches(void);


/**
 * @brief Delay in microseconds
 * @param[in] usec microseconds to be in delay
 * @return None
 */

bool HcSr04Deinit(gpio_t echo, gpio_t trigger);


/**
 * @brief Delay in microseconds
 * @param[in] usec microseconds to be in delay
 * @return None
 */

bool Led_distancia(int16_t distancia_medida);
/**
 * @brief Delay in microseconds
 * @param[in] usec microseconds to be in delay
 * @return None
 */

bool Led_apagar();
#endif /* TEMPLATE_H_ */



