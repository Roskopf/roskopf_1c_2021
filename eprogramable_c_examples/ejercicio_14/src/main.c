/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
/*==================[macros and definitions]=================================*/
/* Ejercicio 14 */
/* Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres,
 * “apellido” de 20 caracteres y edad.
A.Defina una variable con esa estructura y cargue los campos con sus propios datos.
B.Defina un puntero a esa estructura y cargue los campos con los datos de su compañero
(usando acceso por punteros).
 *
 *  */

typedef struct {
		char nombre [12];
		char apellido [20];
		uint8_t edad;
	} alumno;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	alumno alumna_1;
	alumna_1.edad=25;
	strcpy (alumna_1.nombre, "Ana");
	strcpy (alumna_1.apellido, "Roskopf");


	printf ("Alumno 1 %s\r\n");

	printf ("Apellido: %s\r\n", alumna_1.apellido);
	printf ("Nombre: %s\r\n", alumna_1.nombre);
	printf ("Edad: %d\r\n", alumna_1.edad);


	alumno alumno_2;
	alumno *alumno_2pun= &alumno_2;

	strcpy (alumno_2pun->nombre, "Pepito");
	strcpy (alumno_2pun->apellido, "Flores");
	alumno_2pun->edad=27;

	printf ("Alumno 2 %d\r\n");

	printf("Apellido: %s\r\n",alumno_2pun->apellido);
	printf("Nombre:%d\r\n",alumno_2pun->edad);
	printf("Edad: %s\r\n",alumno_2pun->nombre);

	return 0;
}

/*==================[end of file]============================================*/

