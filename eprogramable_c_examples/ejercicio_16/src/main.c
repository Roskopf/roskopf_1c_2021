/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
/* Ejercicio 16:
 * Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 * Declare cuatro variables sin signo de 8 bits y, utilizando máscaras,
 * rotaciones y truncamiento,
 * cargue cada uno de los bytes de la variable de 32 bits.
 * Realice el mismo ejercicio, utilizando la definición de una “union”.
 *  */

union test{
	struct cada_byte{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	} cada_byte;
	uint32_t  todos_los_bytes;
} union_1;


/*==================[internal functions declaration]=========================*/
int main(void)
{
	uint32_t var = 0x01020304;
	uint8_t a, b, c, d;

	    a=var;
	    printf ("Variable a: %d \r\n", a);

	    b= var >>8;
	    printf ("Variable b: %d \r\n", b);

	    c= var >> 16;
	    printf ("Variable c: %d \r\n", c);

	    d= var >> 24;
	    printf ("Variable d: %d \r\n", d);

	    union_1.todos_los_bytes=0x01020304;
	    printf ("Todos los bytes: 0x%x \r\n", union_1.todos_los_bytes);


	    printf ("Variable a con union: %d \r\n", union_1.cada_byte.byte1);
	    printf ("Variable b con union: %d \r\n", union_1.cada_byte.byte2);
	    printf ("Variable c con union: %d \r\n", union_1.cada_byte.byte3);
	    printf ("Variable d con union: %d \r\n", union_1.cada_byte.byte4);

	return 0;
}

/*==================[end of file]============================================*/

