/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/
/*ejercicio_12*/
/*Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1*/
/*Luego con mascaras, coloque un 0 en el bit 14*/


#define BIT14 14

/*==================[internal functions declaration]=========================*/
void printBits (size_t const size, void const * const ptr)
{
	unsigned char * b= (unsigned char*) ptr;
	unsigned char byte;
	int i, j;
	for (i=size-1; i>=0;i--)
	{
		for(j=7;j>=0;j--)
		{
				byte=(b[i]>>j)&1;
				printf("%u",byte);
		}
	}
	puts("");
}


int main(void)
{

	int16_t nro=(-1);
	const int16_t mascara= ~(1 << BIT14);

	int16_t *d_nro= &nro;  /*inicializo el puntero con valor*/

	printf("El valor del numero es:%d\r\n",nro);
	printf("En binario es:");
	printBits(sizeof (nro),&nro);


	*d_nro= ( *d_nro & mascara);
	printf("El numero con mascara:%d\r\n",*d_nro);
	printf("En binario es:");
	printBits(sizeof (*d_nro),&*d_nro);

	return 0;
}


/*==================[end of file]============================================*/

