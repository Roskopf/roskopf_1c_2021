/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>
//#include "main.h"


/* EJERCICIO A
 * Realice un función que reciba un puntero a una estructura LED como la que
 * se muestra a continuación:
struct leds
{
	uint8_t n_led;        	indica el número de led a controlar
	uint8_t n_ciclos;   	indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    	indica el tiempo de cada ciclo
	uint8_t mode;       	ON, OFF, TOGGLE
} my_leds;

 *  */

/*==================[macros and definitions]=================================*/

enum  num_leds{
	 LED1=1,
	 LED2,
	 LED3
};

enum estados{
	 ON,
	 OFF,
	 TOGGLE
};

typedef struct {
		uint8_t n_led;      /*indica el nro de led a controlar */
		uint8_t n_ciclos;   /*indica la cant de ciclos de encendido/apagado */
		uint8_t periodo;    /*indica el tiempo de cada ciclo */
		uint8_t mode;       /*indica los posibles modos ON, OFF, TOGGLE*/
} leds_t;               /*se define con _t para indicar que es un tipo de dato*/


void funcion_leds(leds_t *n){

	uint8_t i=0;
	uint8_t a=0;

	if ((n->mode) == ON) { /*caso: encendido*/
		printf("MODO ON \r\n");

		if (n->n_led == LED1) {
			printf("Enciende led 1 \r\n");
		} else if (n->n_led == LED2) { /*c se ejecuta el primer if. Mayor eficiencia del codigo*/
			printf("Enciende led 2 \r\n");
		} else if (n->n_led == LED3) {
			printf("Enciende led 3 \r\n");
		}

	}

	else if ((n->mode) == OFF) { /*caso: apagado*/
			printf("MODO OFF \r\n");
			if (n->n_led == LED1) {
				printf("Apaga led 1 \r\n");
			}
			else if(n->n_led == LED2) {
				printf("Apaga led 2 \r\n");
			}
			else if(n->n_led == LED3){
				printf("Apaga led 3 \r\n");
			}
		}


		else if ((n->mode) == TOGGLE) {/*caso: toggle*/
			printf("MODO TOGGLE \r\n");

			for (i = 0; i < n->n_ciclos; i++) {

				if (n->n_led == LED1) {
					printf("Se togglea el led1 \r\n");
				}

				if (n->n_led == LED2) {
					printf("Se togglea led 2 \r\n");
				}

				if (n->n_led == LED3) {
					printf("Se togglea led 3 \r\n");
				}


				for (a = 0; a< n->periodo; a++){
											printf("Retardo \r\n");
				}
			}
		}
};


/*==================[internal functions declaration]=========================*/

int main(void)
{

	leds_t led1;

	led1.mode=TOGGLE;
	led1.n_ciclos=2;
	led1.n_led=2;
	led1.periodo=5;

	funcion_leds(&led1);

	return 0;
}

/*==================[end of file]============================================*/
