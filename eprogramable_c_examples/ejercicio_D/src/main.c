/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* EJERCICIO D) Escribir una función que reciba como parámetro un dígito BCD y
 * un vector de estructuras del tipo  gpioConf_t.

typedef struct
{	uint8_t port;
    uint8_t pin;
	uint8_t dir;
} gpioConf_t;

Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
*/

/*==================[inclusions]=============================================*/
#include "../../ejercicio_D/inc/main.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*==================[macros and definitions]=================================*/

#define BIT 4

typedef struct{
   	uint8_t port;				/*!< GPIO port number */
    uint8_t pin;				/*!< GPIO pin number */
   	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
   } gpioConf_t;



void Port(uint8_t digit, gpioConf_t *vector){
	uint8_t i;
	for(i=0; i<BIT; i++){
		if(digit & (1<< i)){
			printf("El puerto ");
			printf("%d",vector[i].port);
			printf(".");
			printf("%d",vector[i].pin);
			printf(":está en 1 \r\n");
		}
		else{
			printf("El puerto ");
			printf("%d",vector[i].port);
			printf(".");
			printf("%d",vector[i].pin);
			printf(":está en 0 \r\n");
		}
	}
}

/*==================[internal functions declaration]=========================*/

int main(void)
{

	gpioConf_t vector_gpio[BIT] = { { 1, 4, 1 }, { 1, 5, 1 }, { 1, 6, 1 }, { 2, 14, 1 } };

	uint8_t digito = 20;
	Port(digito, vector_gpio);

	return 0;
}

/*==================[end of file]============================================*/
