/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/* EJERCICIO C
 Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida
 y un puntero a un arreglo donde se almacene los n dígitos.
 La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos
 de salida en el arreglo pasado como puntero.

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{

}
*/

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"
/*==================[macros and definitions]=================================*/

int8_t BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){

	uint8_t i;
	uint8_t resto;

	for(i=0; i<digits; i++){
		resto=data % 10; /*calculo del resto de la division por 10*/
		data=data/10;
		bcd_number[i]=resto;
	}
};

/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint32_t dato=1024;
	uint8_t nro_digitos=4;
	uint8_t bcd[nro_digitos];
	BinaryToBcd(dato,nro_digitos,bcd);

	uint8_t a;
	for (a=0; a<nro_digitos; a++){
		printf("%d ",a);
		printf("digito %d\r\n",bcd[a]);
	}

	return 0;
}

/*==================[end of file]============================================*/
